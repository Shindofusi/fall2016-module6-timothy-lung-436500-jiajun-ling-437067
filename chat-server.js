// Require the packages we will use:
var http = require("http"),
	socketio = require("socket.io"),
	fs = require("fs");
 
// Listen for HTTP connections.  This is essentially a miniature static file server that only serves our one file, client.html:
var app = http.createServer(function(req, resp){
	// This callback runs when a new connection is made to our HTTP server.
 
	fs.readFile("client.html", function(err, data){
		// This callback runs when the client.html file has been read from the filesystem.
 
		if(err) return resp.writeHead(500);
		resp.writeHead(200);
		resp.end(data);
	});
});
app.listen(3456);
 
var numUsers = 0;
var rooms = {};//object that hold rooms
var users = {};//object that holds users
// Do the Socket.IO magic:
var io = socketio.listen(app);
io.sockets.on("connection", function(socket){
	// This callback runs when a new Socket.IO connection is established.
	socket.on('new_user', function(data){//check for new user
		if(!users[data]){
			console.log("new_user: "+data);
			numUsers++;
			var temp = [];
			temp.push(socket);
			var block_mute = [];
			temp.push(block_mute);
			users[data] = temp;//add the socket of user to array so we can call on that later
			socket.username = data;
			socket.emit("success");//tell client that user was succesffuly added.
			socket.emit("rooms", rooms);//tell client rooms available.
		}
	});
	socket.on('kick', function(data){//temporarily remove a user from a room.
		var temp = rooms[socket.room];
		var index = temp.indexOf(data);
		console.log("index" + index);		
		if(users[data] != null && index != -1 && data != socket.username){//check if user exits, in room, and not creator
			var user_array = users[data];
			user_array[0].leave(socket.room);
			user_array[0].room = null;
			console.log(data + ": " + users[data].room);
			temp.splice(index, 1);//remove user from array
			rooms[socket.room] = temp;
			users[data] = user_array;
			user_array[0].emit("kicked");//tell user he was kicked
			io.sockets.in(socket.room).emit("update_users", temp);//update rest of users
		}
	});
	socket.on('ban', function(data){//permanantly ban a user from room
		//change new_room[1] to ban flag.
		if(users[data] != null && data != socket.username){//check if user exists and is not yourself.
			var temp = rooms[socket.room];
			temp[1].push(data);
			var index = temp.indexOf(data);
			if(index != -1){//check if user is in room and then doing kick function.
				var user_array = users[data];
				user_array[0].leave(socket.room);
				user_array[0].room = null;
				temp.splice(index, 1);
				rooms[socket.room] = temp;
				users[data] = user_array;
				user_array[0].emit("banned");//tell user that he was banned.
				io.sockets.in(socket.room).emit("update_users", temp);//update rest of users in room.
			}
		}
	});
	socket.on("block", function(data){
		var user_array = users[socket.username];
		if(users[data] != null && data != socket.username){
			user_array[1].push(data);
			users[socket.username] = user_array;
		}
	});
	socket.on("mute", function(data){
		if(users[data] != null && data != socket.username){
			var user_array = users[data];
			user_array[1].push(socket.username);
			users[data] = user_array;
		}
	});
	socket.on('create_room_server', function(data, pass) {//create room
		if(rooms[data] == null){//check if room exists
			console.log("new_room: "+data);
			var new_room = [];
			new_room[0] = pass;//first is pass
			new_room[1] = [];//second is user banned list
			new_room[2] = socket.username;//creator, and everything after are just users in room
			rooms[data] = new_room;
			if(rooms[socket.room] != null){//check if user is already in room and if so leave
 				var temp = rooms[socket.room];
 				var index = temp.indexOf(socket.username);
 				temp.splice(index, 1);
 				io.sockets.in(socket.room).emit("update_users", temp);
 			}
			socket.leave(socket.room);
			socket.join(data);
			socket.room = data;
			io.sockets.in(socket.room).emit("create_success");//tell user successful and make him moderator
			io.sockets.emit("rooms", rooms);//tell all users room list.
		}
	});
 	socket.on('join_room_server', function(data, pass){//joining a room.
 		if(rooms[data] != null && data != socket.room){	//check if room exists and you are not already in room
 			var room = rooms[data];
 			var banned = room[1];
 			var index = banned.indexOf(socket.username);
 			console.log("banned: " + banned);
 			if(room[0] == pass && index == -1){//check if password is correct and user is not banned
 				room.push(socket.username);
 				rooms[data] = room;
 				if(rooms[socket.room] != null){//check if user is already in room, if so leave
 					console.log("Old Room: " + socket.room);
 					var temp = rooms[socket.room];
 					var index = temp.indexOf(socket.username);
 					temp.splice(index, 1);
 					io.sockets.in(socket.room).emit("update_users", temp);
 					console.log("updated old room");
 				}
 				socket.leave(socket.room);
 				socket.join(data);
 				socket.room = data;
 				socket.emit("join_success");//tell user successful join
 				io.sockets.in(data).emit("update_users", room);//update all users in list of new users
 				console.log("joined_room: "+rooms[data]);
 			}
 		}
 	});
 	socket.on('private_to_server', function(data, user) {//private messaging.
 		var user_array = users[user];
 		var other_array = users[socket.username];
 		if(user != socket.username && users[user] != null && user_array[0].room == socket.room){//check if user is not self, user exists, and in same room
 			data["message"] = socket.username + " (Private to " + user + "): " + data["message"];
 			var temp = other_array.indexOf(user);
 			if(temp == -1){
 				socket.emit("message_to_client",{message:data["message"]}, other_array[1]);//send message to self
 				user_array[0].emit("message_to_client",{message:data["message"]}, other_array[1]);//send message to user
 			}
 		}
 	});
	socket.on('message_to_server', function(data) {//message everyone in room
		// This callback runs when the server receives a new message from the client.
 		if(socket.username){//check if is user
 			var user_array = users[socket.username];
			console.log("message: "+data["message"]); // log it to the Node.JS output
			data["message"] = socket.username + ": " + data["message"];
			io.sockets.in(socket.room).emit("message_to_client",{message:data["message"]}, user_array[1]); //message everyone in room
		}
	});
	socket.on('disconnect', function(){//disconnect user if he leaves/closes browser
		delete users[socket.username];//delete him from users list.
		if(socket.room){//check if he was in a room
			var room = rooms[socket.room];
			var index = room.indexOf(socket.username);
			if(index != -1){//check room for username
				room.splice(index, 1);//delete username
				io.sockets.in(socket.room).emit("update_users", room);
			}
			rooms[socket.room] = room;
			socket.leave(socket.room);
			socket.room = null;
			console.log("User left: " + socket.username);
		}
	});
});
