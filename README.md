Timothy Lung - 436500
JiaJun Ling - 437067

http://ec2-54-243-13-140.compute-1.amazonaws.com:3456/

We can block and mute certain users we choose. So for block, the user we block (as long as he exists) won't be able to see any of our messages. For mute, the user we mute (as long as he exists) won't be able to message us.

